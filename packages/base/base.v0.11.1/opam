opam-version: "2.0"
maintainer: "opensource@janestreet.com"
authors: ["Jane Street Group, LLC <opensource@janestreet.com>"]
homepage: "https://github.com/janestreet/base"
bug-reports: "https://github.com/janestreet/base/issues"
dev-repo: "git+https://github.com/janestreet/base.git"
license: "Apache-2.0"
build: [
  ["jbuilder" "build" "-p" name "-j" jobs]
]
depends: [
  "ocaml" {>= "4.04.1"}
  "sexplib0" {>= "v0.11" & < "v0.12"}
  "jbuilder" {build & >= "1.0+beta18.1"}
]
depopts: [
  "base-native-int63"
]
synopsis: "Full standard library replacement for OCaml"
description: """
Full standard library replacement for OCaml

Base is a complete and portable alternative to the OCaml standard
library. It provides all standard functionalities one would expect
from a language standard library. It uses consistent conventions
across all of its module.

Base aims to be usable in any context. As a result system dependent
features such as I/O are not offered by Base. They are instead
provided by companion libraries such as stdio:

  https://github.com/janestreet/stdio"""
url {
  src:
    "https://github.com/janestreet/base/releases/download/v0.11.1/base-v0.11.1.tbz"
  checksum: [
    "md5=e7e7dc5db3f1fea19d74a31bbd4ac621"
    "sha256=0cbc10c73183a4935092183609e355252253d3d0ed05084293528d7c4874a3af"
    "sha512=37bf33401dbe2e5433d259dce877f0bd6c9a3bc4a34d9d308556fffa2c0f596c767e6545a3a5d95c895144cc8c53426f0ced14220839f2eb17177b9bffa21c4d"
  ]
}
