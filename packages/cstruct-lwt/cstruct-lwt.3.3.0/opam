opam-version: "2.0"
maintainer:   "anil@recoil.org"
authors:      ["Anil Madhavapeddy" "Richard Mortier" "Thomas Gazagnaire"
               "Pierre Chambart" "David Kaloper" "Jeremy Yallop" "David Scott"
               "Mindy Preston" "Thomas Leonard" ]
homepage:     "https://github.com/mirage/ocaml-cstruct"
license:      "ISC"
dev-repo: "git+https://github.com/mirage/ocaml-cstruct.git"
bug-reports:  "https://github.com/mirage/ocaml-cstruct/issues"
doc: "https://mirage.github.io/ocaml-cstruct/"
tags: [ "org:mirage" "org:ocamllabs" ]
build: [
  ["dune" "subst"] {pinned}
  ["dune" "build" "-p" name "-j" jobs]
]
depends: [
  "ocaml" {>= "4.03.0"}
  "base-unix"
  "lwt"
  "cstruct" {>="3.2.0"}
  "dune" {build & >= "1.0"}
]
synopsis: "Access C-like structures directly from OCaml"
description: """
Cstruct is a library and syntax extension to make it easier to access C-like
structures directly from OCaml.  It supports both reading and writing to these
structures, and they are accessed via the `Bigarray` module."""
url {
  src:
    "https://github.com/mirage/ocaml-cstruct/releases/download/v3.3.0/cstruct-v3.3.0.tbz"
  checksum: [
    "md5=7451389f3941d938575f8887c55c5e6b"
    "sha256=ad44745fc5bd637f8588d35d1d449d7b924caeedd8135babb62b27dd05ccc0ab"
    "sha512=faf284dad5fd191f562f2ae7a08c42ffcf68c3f5ec711eb92a7fbc1f832194a52d9038deb189fc96793c419d9000b468f25448b76c9f6dd929c81c8b9c00d66d"
  ]
}
